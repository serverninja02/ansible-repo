#!/usr/bin/python
from ansible.module_utils.basic import *

#Function to return the default virtual machine folder setting
def getDefaultMachineFolder(vboxCmd, module):
    cmd = vboxCmd + " list systemproperties"
    rc, output, error = module.run_command(cmd)

    #Find the "Default machine folder:" line and get config
    arrOutput = output.split('\n')
    match = [x for x in arrOutput if re.search('^Default machine folder:', x)]
    strFolder = match[0].split(':')[1].strip()

    return strFolder

#Function for creating VMs
def createVM(vboxCmd, module):
    vdiskFile = createHDD(vboxCmd, module)
    osType = module.params['ostype']
    vmName = module.params['name']
    mem = module.params['mem']
    description = module.params['description']
   
    cmd = vboxCmd + " createvm --name '%s' --ostype '%s' --register" % (vmName, osType)
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " modifyvm '%s' --ioapic on" % vmName
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " modifyvm '%s' --description '%s'" % (vmName, description)
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " modifyvm '%s' --boot1 dvd --boot2 disk --boot3 none --boot4 none" % vmName
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " modifyvm '%s' --memory %d" % (vmName, mem)
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " modifyvm '%s' --nic1 bridged --bridgeadapter1 en0" % vmName
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " storagectl '%s' --name 'SATA Controller' --add sata --controller IntelAHCI" % vmName
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " storagectl '%s' --name 'IDE Controller' --add ide" % vmName
    rc, output, error = module.run_command(cmd)

    cmd = vboxCmd + " storageattach '%s' --storagectl 'SATA Controller' --port 0 --device 0 --type hdd --medium '%s'" % (vmName, vdiskFile)
    rc, output, error = module.run_command(cmd)

    
    cmd = vboxCmd + " storageattach '%s' --storagectl 'IDE Controller' --port 0 --device 0 --type dvddrive --medium emptydrive" % (vmName)
    rc, output, error = module.run_command(cmd)

#Function for creating HDD
def createHDD(vboxCmd, module):
    vmName = module.params['name']
    vmDir = module.params['defaultMachineFolder'] + '/' + vmName
    vdiskFile = vmDir + '/' + vmName + '.vdi' 
    os.mkdir(vmDir)
    cmd = vboxCmd + " createhd --filename '%s' --size %d" % (vdiskFile, module.params['hardDisk'])
    rc, output, error = module.run_command(cmd)
    return vdiskFile

#See if name matches output of VM List from VBoxManage
def getVMInfo(vboxCmd, module):
    #module.run_command returns a tuple with the return code, output, and stderror:
    cmd = vboxCmd + " showvminfo '%s'" % module.params['name']
    rc, output, error = module.run_command(cmd)

    for line in output.split('\n'):
        arrLine = line.split(':')
        #Find all mac addresses
        for num in range(1, 37):
            if arrLine[0].find('NIC ' + str(num)) > -1 and arrLine[1].find('MAC') > -1:
                macAddress = arrLine[2].split(',')[0].strip()
                module.params['macaddress' + str(num)] = macAddress

#See if name matches output of VM List from VBoxManage
def checkForVM(vboxCmd, module):
    #module.run_command returns a tuple with the return code, output, and stderror:
    cmd = vboxCmd + ' list vms'
    rc, output, error = module.run_command(cmd)

    #the output line endings are replaced with \n
    for line in output.split('\n'):
        arrLine = line.split('" ')
        name = arrLine[0].replace('"', '').strip()
        if name == module.params['name']:
            uuid = arrLine[1]
            module.params['uuid'] = uuid
            return True

    return False

def getVboxCmd():
    for path in ['/usr/local/bin/VBoxManage', '/usr/bin/VBoxManage']:
        if os.path.isfile(path):
            return path

    return None

def main():
    fields = {
        "name": {"required": True, "type": "str"},
        "description": {"required": False, "type": "str"},
        "hardDisk": {"required": True, "type": "int"},
        "cpus": {"required": True, "type": "int"},
        "mem": {"required": True, "type": "int"},
        "ostype": {"required": True, "type": "str"},
    }

    module = AnsibleModule(argument_spec=fields)
    vboxCmd = getVboxCmd()
    if vboxCmd == None:
        module.fail_json(msg="VBoxManage command NOT FOUND!!!")
   

    #Get Machine Folder
    defaultMachineFolder = getDefaultMachineFolder(vboxCmd, module)
    module.params['defaultMachineFolder'] = defaultMachineFolder

    #Check to see if VM exists
    vmMatch = checkForVM(vboxCmd, module) 
    
    #If we found the VM, exit successfully
    if vmMatch:
        getVMInfo(vboxCmd, module)
        module.exit_json(changed=False, meta=module.params)
    else:
        createVM(vboxCmd, module)
        vmMatch = checkForVM(vboxCmd, module) 
        if vmMatch:
            getVMInfo(vboxCmd, module)
            module.exit_json(changed=True, meta=module.params)
        else:
            module.fail_json(msg="VM NOT FOUND!!!")

if __name__ == '__main__':  
    main()
