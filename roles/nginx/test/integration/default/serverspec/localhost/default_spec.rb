require 'spec_helper'

describe 'nginx::default' do
  it 'Should install Nginx' do
    expect(package('nginx')).to be_installed
  end

  it 'Should remove symlink for default site' do
    expect(file('/etc/nginx/sites-enabled/default')).to_not be_symlink
  end

end
