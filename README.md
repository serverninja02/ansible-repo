Ansible-repo
============
This is my ansible repo for my home infrastructure. I'm putting this together to automate my home infrastructure build and also as a learning project for ansible.

Requirements:
-------------
 - Ansible 2.1.x or greater
 - The ansible tasks are configured with the assumption you're configuring against a stock / minimal install of Ubuntu 14.04
 - **Uses librarian-ansible**: 
   - Requires ruby.
   - Do a `bundle install`
   - Then do `librarian-ansible install`

Hosts:
------
The "production" file contains the hosts

Roles:
------
 - hypervisors - Builds / configures a headless virtualbox server with phpvirtualbox on nginx / php-fpm
 - pxeservers - Builds a pxe kickstart server with tftpd-hpa, nfs, nginx. It also downloads and mounts iso files for Ubuntu 14.04 and Centos 7 as well as adding the kickstart / pxelinux configs to use them for installing minimul linux builds over pxe.

Usage:
======
 - Prepare a host that has a minimum of 4-8GB of RAM with a minimal install of Ubuntu 14.04
 - Edit the `production` file
 - Run the following to deploy machine state with ansible:

**NOTE: This assumes you have ubuntu user with passwordless sudo**
```
ansible-playbook -u ubuntu -k -b -i production site.yml
```

**Edit the production-vms.yml and use the TestVM example to create a task for each VM you wish to build on the hypervisor**
```
ansible-playbook -u ubuntu -k -b -i production production-vms.yml
```

Additional info:
================

Virtualbox library
------------------
I created a library that can be used to create VMs on the headless virtualbox. It needs a little work but the example in production-vms.yml will create and configure a VM for you.

Pxe Kickstart Server:
---------------------
Once you configure with the pxeservers ansible role in this repo, point your DHCP server to this and you can boot and install linux!

DHCP options required:
 - Option 66: The IP / DNS name of the pxe kickstart server
 - Option 67: "pxelinux.0"

Logins to minimal install of Centos / Ubuntu:

**CentOS**
 - user: centos
 - pass: centos

**Ubuntu**
 - user: ubuntu
 - pass: ubuntu

Both users have passwordless sudo privs. These are just some default passwords so its up to you to change their passwords for good security.

To Come:
========
 - Extend Virtualbox library so that it creates a pxe boot file for the mac address of the VM created and starts the created machine to do a hands-off install of linux 
 - DHCP
 - DNS
 - Samba4 AD Domain Controller
 - Other fun stuff

License:
========
Copyright [2016] [John W. Reed II]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
